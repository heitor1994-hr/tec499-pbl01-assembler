# Block 5 - J, JAL, JR

.text
    li $t0, 65536           # load controller's address

    addi $s0, $zero, 1      # s0 = 1
    jal jump_link           # jump to jump_link
    sw $s0, 0($t0)          # controller <- 1 (send s0 to controller)

    addi $s0, $zero, 2      # s0 = 2
    j exit

jump_link:
    sw $ra, 0($t0)          # controller <- 16 (send ra to controller)
    jr $ra

exit:  
    sw $s0, 0($t0)          # controller <- 2 (send s0 to controller)
    addi $s0, $s0, 1        # s0 = 3
    sw $s0, 0($t0)          # controller <- 3 (send s0 to controller)


