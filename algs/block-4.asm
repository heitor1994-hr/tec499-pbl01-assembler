# Block 4 - BEQ, BNE

.text
    li $t0, 65536           # load controller's address

    #BEQ
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 0      # s1 = 0
    beq $s0, $s1, L1        # goto L1  if (1 == 0)
    addi $s2, $zero, 1      # s2 = 1
L1: 
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #BNE
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 0      # s1 = 0
    bne $s0, $s1, L2        # goto L2  if (1 != 0)
    addi $s2, $zero, 1      # s2 = 1
L2: 
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #BEQ
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 1      # s1 = 1
    beq $s0, $s1, L3        # goto L3  if (1 == 1)
    addi $s2, $zero, 1      # s2 = 1
L3: 
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #BNE
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 2      # s0 = 2
    addi $s1, $zero, 2      # s1 = 2
    bne $s0, $s1, L4        # goto L4  if (2 != 2)
    addi $s2, $zero, 1      # s2 = 1
L4: 
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    