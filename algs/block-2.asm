# Block 2 - LUI, SLL, ORI

.text
    li $t0, 65536           # load controller's address

    #LUI
    lui  $s2, 1             # s2 = 1 << 16
    sw $s2, 0($t0)          # controller <- 65536 (send s2 to controller)

    #ORI
    addi $s0, $zero, 0      # s0 = 0
    ori $s2, $s0, 1         # s2 = 0 | 1
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #SLL
    addi $s0, $zero, 2      # s0 = 2
    sll $s2, $s0, 1         # s2 = 2 << 1
    sw $s2, 0($t0)          # controller <- 4 (send s2 to controller)

    #LUI
    lui  $s2, 10            # s2 = 10 << 16
    sw $s2, 0($t0)          # controller <- 655360 (send s2 to controller)

    #ORI
    addi $s0, $zero, 2      # s0 = 2
    ori $s2, $s0, 1         # s2 = 2 | 1
    sw $s2, 0($t0)          # controller <- 3 (send s2 to controller)

    #SLL
    addi $s0, $zero, 1      # s0 = 1
    sll $s2, $s0, 14        # s2 = 1 << 14
    sw $s2, 0($t0)          # controller <- 16384 (send s2 to controller)
    
    