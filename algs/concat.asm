.data
    my_data: .word 20
    L1: .word 5
    C1: .word 3
    matrix_A: .word -2302, 5044, -9846, -4110, -9006, 217, -6515, -7833, 6929, 6401, 5780, 852, 6708, 7448, -2261
    L2: .word 3
    C2: .word 5
    matrix_B: .word -1060, -5394, 2734, -330, 1785, -9643, -1394, 1295, -495, -3087, 3741, -9249, 8810, -4158, -3182
    matrix_RES: .word 0

.text

    li $t0, 65536           # load controller's address

    # BLOCK 1
    #ADD
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 2      # s1 = 2
    add $s3, $s0, $s1       # s3 = 1 + 2
    sw $s3, 0($t0)          # controller <- 3 (send s3 to controller)

    #ADDI
    addi $s0, $zero, 50     # s0 = 50
    addi $s3, $s0, 60       # s3 = 50 + 60
    sw $s3, 0($t0)          # controller <- 110 (send s3 to controller)

    #SUB
    addi $s0, $zero, 10     # s0 = 10
    addi $s1, $zero, 5      # s1 = 5
    sub $s3, $s0, $s1       # s3 = 10 - 5
    sw $s3, 0($t0)          # controller <- 5 (send s3 to controller)

    #MUL
    addi $s0, $zero, 3      # s0 = 3
    addi $s1, $zero, 3      # s1 = 3
    mul $s3, $s0, $s1       # s3 = 3*3
    sw $s3, 0($t0)          # controller <- 9 (send s3 to controller)

    #SLT
    addi $s0, $zero, -1     # s0 = -1
    addi $s1, $zero, 0      # s1 = 0
    slt $s3, $s0, $s1       # s3 = (-1 < 0) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 1 (send s3 to controller)

    #ADD
    addi $s0, $zero, -100   # s0 = -100
    addi $s1, $zero, 100    # s1 = 100
    add $s3, $s0, $s1       # s3 = -100 + 100
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)
    
    #ADDI
    addi $s0, $zero, 30000  # s0 = 30000
    addi $s3, $s0, 30000    # s3 = 30000 + 30000
    sw $s3, 0($t0)          # controller <- 60000 (send s3 to controller)

    #SUB
    addi $s0, $zero, -500   # s0 = -500
    addi $s1, $zero, 500    # s1 = 500
    sub $s3, $s0, $s1       # s3 = -500 - 500
    sw $s3, 0($t0)          # controller <- -1000 (send s3 to controller)

    #MUL
    addi $s0, $zero, 20000  # s0 = 20000
    addi $s1, $zero, 20000  # s1 = 20000
    mul $s3, $s0, $s1       # s3 = 20000*20000
    sw $s3, 0($t0)          # controller <- 40.0000.000 (send s3 to controller)

    #SLT
    addi $s0, $zero, 64     # s0 = 64
    addi $s1, $zero, 0      # s1 = 0
    slt $s3, $s0, $s1       # s3 = (64 < 0) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    #ADD
    addi $s0, $zero, 0      # s0 = 0
    addi $s1, $zero, 0      # s1 = 0
    add $s3, $s0, $s1       # s3 = 0 + 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)
    
    #ADDI
    addi $s0, $zero, -15000 # s0 = -15000
    addi $s3, $s0, 30000    # s3 = -15000 + 30000
    sw $s3, 0($t0)          # controller <- 15000 (send s3 to controller)

    #SUB
    addi $s0, $zero, -5     # s0 = -5
    addi $s1, $zero, -5     # s1 = -5
    sub $s3, $s0, $s1       # s3 = -5 - (-5)
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    #MUL
    addi $s0, $zero, -8     # s0 = -8
    addi $s1, $zero, -8     # s1 = -8
    mul $s3, $s0, $s1       # s3 = (-8)*(-8)
    sw $s3, 0($t0)          # controller <- 64 (send s3 to controller)

    #SLT
    addi $s0, $zero, 128    # s0 = 128
    addi $s1, $zero, 128    # s1 = 128
    slt $s3, $s0, $s1       # s3 = (128 < 128) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    # BLOCK 2
    #LUI
    lui  $s2, 1             # s2 = 1 << 16
    sw $s2, 0($t0)          # controller <- 65536 (send s2 to controller)

    #ORI
    addi $s0, $zero, 0      # s0 = 0
    ori $s2, $s0, 1         # s2 = 0 | 1
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #SLL
    addi $s0, $zero, 2      # s0 = 2
    sll $s2, $s0, 1         # s2 = 2 << 1
    sw $s2, 0($t0)          # controller <- 4 (send s2 to controller)

    #LUI
    lui  $s2, 10            # s2 = 10 << 16
    sw $s2, 0($t0)          # controller <- 655360 (send s2 to controller)

    #ORI
    addi $s0, $zero, 2      # s0 = 2
    ori $s2, $s0, 1         # s2 = 2 | 1
    sw $s2, 0($t0)          # controller <- 3 (send s2 to controller)

    #SLL
    addi $s0, $zero, 1      # s0 = 1
    sll $s2, $s0, 14        # s2 = 1 << 14
    sw $s2, 0($t0)          # controller <- 16384 (send s2 to controller)

    # BLOCK 3
    #DIV, MFHI
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 1 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 5      # s0 = 5
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 5 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 10     # s0 = 10
    addi $s1, $zero, 20     # s1 = 20
    div $s0, $s1            # hi = 10 % 20
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 10 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 3      # s0 = 3
    addi $s1, $zero, 3      # s1 = 3
    div $s0, $s1            # hi = 3 % 3
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 4      # s0 = 4
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 4 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 10000  # s0 = 10000
    addi $s1, $zero, 10001  # s1 = 10001
    div $s0, $s1            # hi = 10000 % 10001
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 10000 (send s2 to controller)

    # BLOCK 4
    #BEQ
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 0      # s1 = 0
    beq $s0, $s1, L_1        # goto L1  if (1 == 0)
    addi $s2, $zero, 1      # s2 = 1
L_1: 
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #BNE
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 0      # s1 = 0
    bne $s0, $s1, L_2        # goto L2  if (1 != 0)
    addi $s2, $zero, 1      # s2 = 1
L_2: 
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #BEQ
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 1      # s1 = 1
    beq $s0, $s1, L_3        # goto L3  if (1 == 1)
    addi $s2, $zero, 1      # s2 = 1
L_3: 
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #BNE
    addi $s2, $zero, 0      # s2 = 0
    addi $s0, $zero, 2      # s0 = 2
    addi $s1, $zero, 2      # s1 = 2
    bne $s0, $s1, L_4        # goto L4  if (2 != 2)
    addi $s2, $zero, 1      # s2 = 1
L_4: 
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    # BLOCK 5
    addi $s0, $zero, 1      # s0 = 1
    jal jump_link           # jump to jump_link
    sw $s0, 0($t0)          # controller <- 1 (send s0 to controller)

    addi $s0, $zero, 2      # s0 = 2
    j exit

jump_link:
    sw $ra, 0($t0)          # controller <- 524 (send ra to controller)
    jr $ra

exit:  
    sw $s0, 0($t0)          # controller <- 2 (send s0 to controller)
    addi $s0, $s0, 1        # s0 = 3
    sw $s0, 0($t0)          # controller <- 3 (send s0 to controller)

    # FIBONACCI
    li $a0, 17						# n = 17 ,  coloque o valor de n desejado nesta linha
    jal fib							# fib(n)
    j exit_2						# pula para o fim do programa
   
fib:
    li $t1, 1						# t1 = 1
    slt		$t2, $t1, $a0			# if (t1 < n) t2 = 1 else t2 = 0
    bne		$t2, $zero, fib_recurse # if (t2 != 0) goto fib_recuse
    move    $v0, $a0				# v0 = n
    jr  $ra							# goto endereco de retorno
 
fib_recurse:
    addi $sp, $sp, -12				# libera 3 espacos na pilha
    sw $ra,0($sp)      				# salva endereco de retorno na pilha
    sw  $a0, 4($sp)					# salva n na pilha
 
    addi     $a0, $a0, -1       	# n = n - 1
    jal     fib						# fib(n-1)
    lw      $a0,4($sp)     			# carrega n da pilha
    sw  $v0, 8($sp)        			# salva o resultado de fib(n-1) na pilha
   
    addi $a0, $a0, -2        		# n = n - 2
    jal fib							# fib(n-2)
 
    lw  $t3, 8($sp)      			# carrega o resultado de fib(n-1) da pilha
    add $v0, $t3, $v0        		# soma o resultado de fib(n-2) com fib(n-1) e guarda o resultado em v0
 
    lw  $ra, 0($sp)      			# carrega o endereco de retorno da pilha
    addi $sp, $sp, 12         		# restaura o ponteiro da pilha
    jr  $ra							# goto endereco de retorno
 
exit_2: 
    sw $v0, 0($t0)                  # controller <- 1597
    
    # MATRIX MULTIPLY
    la $t1, L1                      # t1 = address(L1)
    lw $s0, 0($t1)                  # s0 = L1
    la $t1, C1                      # t1 = address(C1)
    lw $s1, 0($t1)                  # s1 = C1
    la $s2, matrix_A                # s2 = address(matrix_A)

    la $t1, L2                      # t1 = address(L2)
    lw $s3, 0($t1)                  # s3 = L2
    la $t1, C2                      # t1 = address(C2)
    lw $s4, 0($t1)                  # s4 = C2
    la $s5, matrix_B                # s5 = address(matrix_B)

    la $s6, matrix_RES              # s6 = address(matrix_RES)

    li $t1, 0                       # i = 0

LOOP1:
    li $t2, 0                       # j = 0

LOOP2:
    li $t3, 0                       # k = 0

LOOP3:
    mul $t4, $t1, $s4               # t4 = i * C2
    add $t4, $t4, $t2               # t4 = t4 + j
    sll $t4, $t4, 2                 # t4 = t4 * 4
    add $t4, $t4, $s6               # t4 = t4 + address(matrix_RES)
    lw $t8, 0($t4)                  # t8 = matrix_RES[i][j]

    mul $t5, $t1, $s1               # t5 = i * C1
    add $t5, $t5, $t3               # t5 = t5 + k
    sll $t5, $t5, 2                 # t5 = t5 * 4
    add $t5, $t5, $s2               # t5 = t5 + address(matrix_A)
    lw $t5, 0($t5)                  # t5 = matrix_A[i][k]

    mul $t6, $t3, $s4               # t6 = k * C2
    add $t6, $t6, $t2               # t6 = t6 + j
    sll $t6, $t6, 2                 # t6 = t6 * 4
    add $t6, $t6, $s5               # t6 = t6 + address(matrix_B)
    lw $t6, 0($t6)                  # t6 = matrix_B[k][j]

    mul $t7, $t5, $t6               # t7 = matrix_A[i][k] * matrix_B[k][j]
    add $t7, $t7, $t8               # t7 = t7 + matrix_RES[i][j]

    sw $t7, 0($t4)                  # matrix_RES[i][j] = t7

    addi $t3, $t3, 1                # k++
    bne $t3, $s1, LOOP3             # if (k != C1) goto LOOP3 

    addi $t2, $t2, 1                # j++
    bne $t2, $s4, LOOP2             # if (j != C2) goto LOOP2

    addi $t1, $t1, 1                # i++
    bne $t1, $s0, LOOP1             # if (i != L1) goto LOOP1

store:
    li $t1, 0                       # i = 0
    mul $t2, $s0, $s4               # t2 = L1*C2
    
LOOP4:
    sll $t5, $t1, 2                 # t5 = i * 4
    add $t3, $t5, $s6               # t3 = t5 + address(matrix_RES)
    lw $t4, 0($t3)                  # t4 = matrix_RES[i]
    sw $t4, 0($t0)                  # controller <- t4

    addi $t1, $t1, 1                # i++
    bne $t1, $t2, LOOP4             # if (i != L1*C2) goto LOOP4

    #FORWARD
    li $s0, 1
    li $s1, 10
    li $s2, 100
    li $s3, 1000

    nop
    nop
    nop

    # Rs MEM forward
    add $t1, $s0, $s1           # t1 = 1 + 10
    add $t2, $t1, $s0           # t2 = t1 + 1
    sw $t2, 0($t0)              # controller <- 12

    nop
    nop
    nop

    # Rt MEM forward
    add $t1, $s1, $s2           # t1 = 10 + 100
    add $t2, $s1, $t1           # t2 = 10 + t1
    sw $t2, 0($t0)              # controller <- 120

    nop
    nop
    nop

    # Rs WB forward
    add $t1, $s0, $s1           # t1 = 1 + 10
    nop
    add $t2, $t1, $s0           # t2 = t1 + 1
    sw $t2, 0($t0)              # controller <- 12

    nop
    nop
    nop

    # Rt WB forward
    add $t1, $s1, $s2           # t1 = 10 + 100
    nop
    add $t2, $s1, $t1           # t2 = t1 + 10
    sw $t2, 0($t0)              # controller <- 120

    nop
    nop
    nop

    # Lw/Sw forward
    la $t1, my_data             # t1 = address(my_data)
    lw $t2, 0($t1)              # t2 = my_data[0]
    sw $t2, 4($t1)              # my_data[1] = t2
    lw $t3, 4($t1)              # t3 = my_data[1]
    sw $t3, 0($t0)              # controller <- 20

    nop
    nop
    nop

    # Branch Rs MEM forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s0, $s0           # t1 = 1 - 1
    beq $t1, $zero, F1          # if (t1 == 0) goto F1
    addi $t2, $zero, 0
F1:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rt MEM forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s1, $s1           # t1 = 10 - 10
    beq $zero, $t1, F2          # if (0 == t1) goto F2
    addi $t2, $zero, 0
F2:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rs WB forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s2, $s2           # t1 = 100 - 100
    nop
    nop
    beq $t1, $zero, F3          # if (t1 == 0) goto F3
    addi $t2, $zero, 0
F3:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rt WB forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s3, $s3           # t1 = 1000 - 1000
    nop
    nop
    beq $zero, $t1, F4          # if (0 == t1) goto F4
    addi $t2, $zero, 0
F4:
    sw $t2, 0($t0)              # controller <- 1

