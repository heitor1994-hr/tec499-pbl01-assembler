# Arithmetic Operations Text
# Matrix Multiply Algorithm

#   | -2302  5044 -9846 |     | -1060 -5394  2734  -330  1785 |     | -83033058  96451306 -86504948  39202548 11650074 |
#   | -4110 -9006   217 |  x  | -9643 -1394  1295  -495 -3087 |  =  |  92013255  32716671 -20987740   4911984 19774678 |
#   | -6515 -7833  6929 |     |  3741 -9249  8810 -4158 -3182 |     | 108360908 -18025209  33088745 -22783497 -9496882 |
#   |  6401  5780   852 |                                           | -59334268 -50464462  32491554  -8516046 -9128139 |
#   |  6708  7448 -2261 |                                           | -87389945 -25653475   8065422   3500838 -3823694 |

.data
    L1: .word 5
    C1: .word 3
    matrix_A: .word -2302, 5044, -9846, -4110, -9006, 217, -6515, -7833, 6929, 6401, 5780, 852, 6708, 7448, -2261
    L2: .word 3
    C2: .word 5
    matrix_B: .word -1060, -5394, 2734, -330, 1785, -9643, -1394, 1295, -495, -3087, 3741, -9249, 8810, -4158, -3182
    matrix_RES: .word 0

.text
    li $t0, 65536                   # load controller's address

    la $t1, L1                      # t1 = address(L1)
    lw $s0, 0($t1)                  # s0 = L1
    la $t1, C1                      # t1 = address(C1)
    lw $s1, 0($t1)                  # s1 = C1
    la $s2, matrix_A                # s2 = address(matrix_A)

    la $t1, L2                      # t1 = address(L2)
    lw $s3, 0($t1)                  # s3 = L2
    la $t1, C2                      # t1 = address(C2)
    lw $s4, 0($t1)                  # s4 = C2
    la $s5, matrix_B                # s5 = address(matrix_B)

    la $s6, matrix_RES              # s6 = address(matrix_RES)

    li $t1, 0                       # i = 0

LOOP1:
    li $t2, 0                       # j = 0

LOOP2:
    li $t3, 0                       # k = 0

LOOP3:
    mul $t4, $t1, $s4               # t4 = i * C2
    add $t4, $t4, $t2               # t4 = t4 + j
    sll $t4, $t4, 2                 # t4 = t4 * 4
    add $t4, $t4, $s6               # t4 = t4 + address(matrix_RES)
    lw $t8, 0($t4)                  # t8 = matrix_RES[i][j]

    mul $t5, $t1, $s1               # t5 = i * C1
    add $t5, $t5, $t3               # t5 = t5 + k
    sll $t5, $t5, 2                 # t5 = t5 * 4
    add $t5, $t5, $s2               # t5 = t5 + address(matrix_A)
    lw $t5, 0($t5)                  # t5 = matrix_A[i][k]

    mul $t6, $t3, $s4               # t6 = k * C2
    add $t6, $t6, $t2               # t6 = t6 + j
    sll $t6, $t6, 2                 # t6 = t6 * 4
    add $t6, $t6, $s5               # t6 = t6 + address(matrix_B)
    lw $t6, 0($t6)                  # t6 = matrix_B[k][j]

    mul $t7, $t5, $t6               # t7 = matrix_A[i][k] * matrix_B[k][j]
    add $t7, $t7, $t8               # t7 = t7 + matrix_RES[i][j]

    sw $t7, 0($t4)                  # matrix_RES[i][j] = t7

    addi $t3, $t3, 1                # k++
    bne $t3, $s1, LOOP3             # if (k != C1) goto LOOP3 

    addi $t2, $t2, 1                # j++
    bne $t2, $s4, LOOP2             # if (j != C2) goto LOOP2

    addi $t1, $t1, 1                # i++
    bne $t1, $s0, LOOP1             # if (i != L1) goto LOOP1

store:
    li $t1, 0                       # i = 0
    mul $t2, $s0, $s4               # t2 = L1*C2
    
LOOP4:
    sll $t5, $t1, 2                 # t5 = i * 4
    add $t3, $t5, $s6               # t3 = t5 + address(matrix_RES)
    lw $t4, 0($t3)                  # t4 = matrix_RES[i]
    sw $t4, 0($t0)                  # controller <- t4

    addi $t1, $t1, 1                # i++
    bne $t1, $t2, LOOP4             # if (i != L1*C2) goto LOOP4
    


    