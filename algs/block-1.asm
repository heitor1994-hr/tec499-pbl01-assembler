# Blcok 1 - ADD, ADDI, SUB, MUL, SLT

.text

    li $t0, 65536           # load controller's address

    #ADD
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 2      # s1 = 2
    add $s3, $s0, $s1       # s3 = 1 + 2
    sw $s3, 0($t0)          # controller <- 3 (send s3 to controller)

    #ADDI
    addi $s0, $zero, 50     # s0 = 50
    addi $s3, $s0, 60       # s3 = 50 + 60
    sw $s3, 0($t0)          # controller <- 110 (send s3 to controller)

    #SUB
    addi $s0, $zero, 10     # s0 = 10
    addi $s1, $zero, 5      # s1 = 5
    sub $s3, $s0, $s1       # s3 = 10 - 5
    sw $s3, 0($t0)          # controller <- 5 (send s3 to controller)

    #MUL
    addi $s0, $zero, 3      # s0 = 3
    addi $s1, $zero, 3      # s1 = 3
    mul $s3, $s0, $s1       # s3 = 3*3
    sw $s3, 0($t0)          # controller <- 9 (send s3 to controller)

    #SLT
    addi $s0, $zero, -1     # s0 = -1
    addi $s1, $zero, 0      # s1 = 0
    slt $s3, $s0, $s1       # s3 = (-1 < 0) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 1 (send s3 to controller)

    #ADD
    addi $s0, $zero, -100   # s0 = -100
    addi $s1, $zero, 100    # s1 = 100
    add $s3, $s0, $s1       # s3 = -100 + 100
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)
    
    #ADDI
    addi $s0, $zero, 30000  # s0 = 30000
    addi $s3, $s0, 30000    # s3 = 30000 + 30000
    sw $s3, 0($t0)          # controller <- 60000 (send s3 to controller)

    #SUB
    addi $s0, $zero, -500   # s0 = -500
    addi $s1, $zero, 500    # s1 = 500
    sub $s3, $s0, $s1       # s3 = -500 - 500
    sw $s3, 0($t0)          # controller <- -1000 (send s3 to controller)

    #MUL
    addi $s0, $zero, 20000  # s0 = 20000
    addi $s1, $zero, 20000  # s1 = 20000
    mul $s3, $s0, $s1       # s3 = 20000*20000
    sw $s3, 0($t0)          # controller <- 40.0000.000 (send s3 to controller)

    #SLT
    addi $s0, $zero, 64     # s0 = 64
    addi $s1, $zero, 0      # s1 = 0
    slt $s3, $s0, $s1       # s3 = (64 < 0) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    #ADD
    addi $s0, $zero, 0      # s0 = 0
    addi $s1, $zero, 0      # s1 = 0
    add $s3, $s0, $s1       # s3 = 0 + 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)
    
    #ADDI
    addi $s0, $zero, -15000 # s0 = -15000
    addi $s3, $s0, 30000    # s3 = -15000 + 30000
    sw $s3, 0($t0)          # controller <- 15000 (send s3 to controller)

    #SUB
    addi $s0, $zero, -5     # s0 = -5
    addi $s1, $zero, -5     # s1 = -5
    sub $s3, $s0, $s1       # s3 = -5 - (-5)
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    #MUL
    addi $s0, $zero, -8     # s0 = -8
    addi $s1, $zero, -8     # s1 = -8
    mul $s3, $s0, $s1       # s3 = (-8)*(-8)
    sw $s3, 0($t0)          # controller <- 64 (send s3 to controller)

    #SLT
    addi $s0, $zero, 128    # s0 = 128
    addi $s1, $zero, 128    # s1 = 128
    slt $s3, $s0, $s1       # s3 = (128 < 128) ? 1 : 0
    sw $s3, 0($t0)          # controller <- 0 (send s3 to controller)

    


