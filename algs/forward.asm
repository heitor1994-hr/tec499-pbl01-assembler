# Test forward unit and branch forward unit

.data
    my_data: .word 20

.text
    li $t0, 65536               # load controller's address

    li $s0, 1
    li $s1, 10
    li $s2, 100
    li $s3, 1000

    nop
    nop
    nop

    # Rs MEM forward
    add $t1, $s0, $s1           # t1 = 1 + 10
    add $t2, $t1, $s0           # t2 = t1 + 1
    sw $t2, 0($t0)              # controller <- 12

    nop
    nop
    nop

    # Rt MEM forward
    add $t1, $s1, $s2           # t1 = 10 + 100
    add $t2, $s1, $t1           # t2 = 10 + t1
    sw $t2, 0($t0)              # controller <- 120

    nop
    nop
    nop

    # Rs WB forward
    add $t1, $s0, $s1           # t1 = 1 + 10
    nop
    add $t2, $t1, $s0           # t2 = t1 + 1
    sw $t2, 0($t0)              # controller <- 12

    nop
    nop
    nop

    # Rt WB forward
    add $t1, $s1, $s2           # t1 = 10 + 100
    nop
    add $t2, $s1, $t1           # t2 = t1 + 10
    sw $t2, 0($t0)              # controller <- 120

    nop
    nop
    nop

    # Lw/Sw forward
    la $t1, my_data             # t1 = address(my_data)
    lw $t2, 0($t1)              # t2 = my_data[0]
    sw $t2, 4($t1)              # my_data[1] = t2
    lw $t3, 4($t1)              # t3 = my_data[1]
    sw $t3, 0($t0)              # controller <- 20

    nop
    nop
    nop

    # Branch Rs MEM forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s0, $s0           # t1 = 1 - 1
    beq $t1, $zero, F1          # if (t1 == 0) goto F1
    addi $t2, $zero, 0
F1:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rt MEM forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s1, $s1           # t1 = 10 - 10
    beq $zero, $t1, F2          # if (0 == t1) goto F2
    addi $t2, $zero, 0
F2:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rs WB forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s2, $s2           # t1 = 100 - 100
    nop
    nop
    beq $t1, $zero, F3          # if (t1 == 0) goto F3
    addi $t2, $zero, 0
F3:
    sw $t2, 0($t0)              # controller <- 1

    nop
    nop
    nop

    # Branch Rt WB forward
    addi $t2, $zero, 1          # t2 = 1
    addi $t1, $zero, 1          # t1 = 1
    sub $t1, $s3, $s3           # t1 = 1000 - 1000
    nop
    nop
    beq $zero, $t1, F4          # if (0 == t1) goto F4
    addi $t2, $zero, 0
F4:
    sw $t2, 0($t0)              # controller <- 1



    