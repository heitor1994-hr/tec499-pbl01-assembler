# Block 3 - DIV, MFHI

.text
    li $t0, 65536           # load controller's address

    #DIV, MFHI
    addi $s0, $zero, 1      # s0 = 1
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 1 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 5      # s0 = 5
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 5 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 1 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 10     # s0 = 10
    addi $s1, $zero, 20     # s1 = 20
    div $s0, $s1            # hi = 10 % 20
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 10 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 3      # s0 = 3
    addi $s1, $zero, 3      # s1 = 3
    div $s0, $s1            # hi = 3 % 3
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 4      # s0 = 4
    addi $s1, $zero, 2      # s1 = 2
    div $s0, $s1            # hi = 4 % 2
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 0 (send s2 to controller)

    #DIV, MFHI
    addi $s0, $zero, 10000  # s0 = 10000
    addi $s1, $zero, 10001  # s1 = 10001
    div $s0, $s1            # hi = 10000 % 10001
    mfhi $s2                # s2 = hi
    sw $s2, 0($t0)          # controller <- 10000 (send s2 to controller)
    
    