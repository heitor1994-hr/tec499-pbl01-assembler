from util import getBinaryRepresentation
import registers
import pseudoinstructions

typeR = {
    'add': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '100000', 
        'defaults': { 'shift': '00000' } },
    'addu': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '100001',
        'defaults': { 'shift': '00000' } },
    'clo': { 'opcode': '000000', 'format': ['rd', 'rs'], 'funct': '010001', 
        'defaults': { 'shift': '00001', 'rt': '00000' }  },
    'clz': { 'opcode': '000000', 'format': ['rd', 'rs'], 'funct': '010000', 
        'defaults': { 'shift': '00001', 'rt': '00000' } },
    'sub': { 'opcode': '000000', 'format': ['rd', 'rs', 'rt'], 'funct': '100010', 
        'defaults':  { 'shift': '00000' } },
    'subu': { 'opcode': '000000', 'format': ['rd', 'rs', 'rt'], 'funct': '100011', 
        'defaults':  { 'shift': '00000' } },
    'seb': { 'opcode': '011111', 'format': ['rd', 'rt'], 'funct': '100000', 
        'defaults': { 'shift': '10000', 'rs': '00000' } },
    'seh': { 'opcode': '011111', 'format': ['rd', 'rt'], 'funct': '100000', 
        'defaults': { 'shift': '11000', 'rs': '00000' }  },
    'sll': { 'opcode': '000000', 'format': ['rd','rt','shift'], 'funct': '000000', 
        'defaults': { 'rs': '00000' } },
    'sllv': { 'opcode': '000000', 'format': ['rd','rt','rs'], 'funct': '000100', 
        'defaults':  { 'shift': '00000' } },
    'sra': { 'opcode': '000000', 'format': ['rd', 'rt', 'shift'], 'funct': '000011', 
        'defaults': { 'rs': '00000'} },
    'srav': { 'opcode': '000000', 'format': ['rd', 'rt', 'rs'], 'funct': '000111', 
        'defaults': { 'shift': '00000' } },
    'srl': { 'opcode': '000000', 'format': ['rd', 'rt', 'shift'], 'funct': '000010', 
        'defaults': { 'rs': '00000' } },
    'srlv': { 'opcode': '000000', 'format': ['rd', 'rt', 'rs' ], 'funct': '000110', 
        'defaults':  { 'shift': '00000' } },
    'and': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '100100', 
        'defaults': { 'shift': '00000' } },
    'nor': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '100111', 
        'defaults': { 'shift': '00000' } },
    'or': { 'opcode': '000000', 'format': ['rd', 'rs', 'rt'], 'funct': '100101', 
        'defaults': { 'shift': '00000' } },
    'xor': { 'opcode': '000000', 'format': ['rd', 'rs', 'rt'], 'funct': '100110', 
        'defaults':  { 'shift': '00000' } },
    'div': { 'opcode': '000000', 'format': ['rs','rt'], 'funct': '011010', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'divu': { 'opcode': '000000', 'format': ['rs','rt'], 'funct': '011011', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'madd': {'opcode': '011100', 'format': ['rs','rt'], 'funct': '000000', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'maddu': { 'opcode': '011100', 'format': ['rs', 'rt'], 'funct': '000001', 
        'defaults':  { 'shift': '00000', 'rd': '00000' } },
    'msub': { 'opcode': '011100', 'format': ['rs', 'rt'], 'funct': '000100', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'msubu': { 'opcode': '011100', 'format': ['rs', 'rt'], 'funct': '000101', 
        'defaults':  { 'shift': '00000', 'rd': '00000' } },
    'mul':  { 'opcode': '011100', 'format': ['rd','rs','rt'], 'funct': '000010', 
        'defaults': { 'shift': '00000' } },
    'mult': { 'opcode': '000000', 'format': ['rs','rt'], 'funct': '011000', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'multu': {'opcode': '000000', 'format': ['rs','rt'], 'funct': '011001', 
        'defaults': { 'shift': '00000', 'rd': '00000' } },
    'mfhi': { 'opcode': '000000', 'format': ['rd'], 'funct': '010000', 
        'defaults': { 'shift': '00000', 'rs': '00000', 'rt': '00000' } },
    'mflo': { 'opcode': '000000', 'format': ['rd'], 'funct': '010010',
        'defaults': { 'shift': '00000', 'rs': '00000', 'rt': '00000' } },
    'mthi': { 'opcode': '000000', 'format': ['rs'], 'funct': '010001', 
        'defaults': { 'shift': '00000', 'rt': '00000', 'rd': '00000' } },
    'mtlo': { 'opcode': '000000', 'format': ['rs'], 'funct': '010011', 
        'defaults': { 'shift': '00000', 'rt': '00000', 'rd': '00000' } },
    'jr': { 'opcode': '000000', 'format': ['rs'], 'funct': '001000', 
        'defaults': { 'shift': '00000', 'rt': '00000', 'rd': '00000' } },
    'movn': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '001011', 
        'defaults': { 'shift': '00000' } },
    'movz': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '001010', 
        'defaults': { 'shift': '00000' } },
    'slt': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '101010', 
        'defaults': { 'shift': '00000' } },
    'sltu': { 'opcode': '000000', 'format': ['rd','rs','rt'], 'funct': '101011', 
        'defaults': { 'shift': '00000' } },
    'jalr': { 'opcode': '000000', 'format': ['rd', 'rs'], 'funct': '001001', 
        'defaults': { 'rt': '00000' } },
    'wsbh': { 'opcode': '011111', 'format': ['rd', 'rt'], 'funct': '100000', 
        'defaults': { 'shift': '00010' } }
}

typeI = {
    'addi': { 'opcode': '001000', 'format': ['rt','rs','immediate'], 
        'defaults': {} },
    'addiu': { 'opcode': '001001', 'format': ['rt', 'rs', 'immediate'], 
        'defaults': {} },
    'lui': { 'opcode': '001111', 'format': ['rt','immediate'], 
        'defaults': {} },
    'andi': { 'opcode': '001100', 'format': ['rt','rs','immediate'], 
        'defaults': {} },
    'ori': { 'opcode': '001101', 'format': ['rt','rs','immediate'], 
        'defaults': {} },
    'xori': { 'opcode': '001110', 'format': ['rt', 'rs', 'immediate'], 
        'defaults': {} },
    'bgez': { 'opcode': '000001', 'format': ['rs','immediate'], 
        'defaults': { 'rt': '00001'} },
    'bltz': { 'opcode': '000001', 'format': ['rs', 'immediate'], 
        'defaults': { 'rt': '00000'} },
    'beq': { 'opcode': '000100', 'format': ['rs','rt', 'immediate'], 
        'defaults': {} },
    'bne': { 'opcode': '000101', 'format': ['rs','rt', 'immediate'], 
        'defaults': {} }, 
    'lb': { 'opcode': '100000', 'format': ['rt','immediate', 'rs'], 
        'defaults': {} },
    'lw': { 'opcode': '100011', 'format': ['rt','immediate','rs'], 
        'defaults': {} },
    'sb': { 'opcode': '101000', 'format': ['rt','immediate','rs'], 
        'defaults': {} },
    'sw': { 'opcode': '101011', 'format': ['rt','immediate','rs'], 
        'defaults': {} },
    'lh': { 'opcode': '100001', 'format': ['rt','immediate','rs'], 
        'defaults': {} },
    'sh': { 'opcode': '101001', 'format': ['rt','immediate','rs'], 
        'defaults': {} },
    'slti': { 'opcode': '001010', 'format': ['rt','rs','immediate'], 
        'defaults': {} },
    'sltiu': { 'opcode': '001011', 'format': ['rt','rs','immediate'], 
        'defaults': {} }
}

typeJ = {
    'j': { 'opcode': '000010', 'format': ['address'], 'defaults': {} },
    'jal': { 'opcode': '000011', 'format': ['address'], 'defaults': {} }
}

allTypes = {}
allTypes.update(typeI)
allTypes.update(typeR)
allTypes.update(typeJ)

branchInsts = [ 'bgez', 'bltz', 'beq', 'bne', 'blt', 'blti', 'bgt']

def isBranch (inst):
    if inst in branchInsts:
        return True
    else:
        return False

def isRinst (inst):
    if inst in typeR:
        return True
    else:
        return False

def isIinst (inst):
    if inst in typeI:
        return True
    else:
        return False

def isJinst (inst):
    if inst in typeJ:
        return True
    else:
        return False


def getInstInfo(mnemonic):
    lowerInst = mnemonic.lower()
    if lowerInst in allTypes:
        return allTypes[lowerInst]

def getInstructionSize(mnemonic):
    lowerInst = mnemonic.lower()
    if lowerInst in allTypes:
        return 1
    elif pseudoinstructions.isPseudoInstruction(lowerInst):
        return pseudoinstructions.getPseudoInstructionSize(lowerInst)


def getInstructionTranslator(inst):
    if isRinst(inst):
        return RTranslator(inst)
    elif isIinst(inst):
        return ITranslator(inst)
    elif isJinst(inst):
        return JTranslator(inst)
    elif pseudoinstructions.isPseudoInstruction(inst):
        return PseudoTranslator(inst)
    else:
        return None

class InstructionCreator:

    def createRinst(self,opcode, rs='00000', rt='00000', rd='00000', shift='00000', funct='000000'):
        return [''.join([opcode,rs,rt,rd,shift,funct])]
    
    def createIinst (self,opcode, rs='00000', rt='00000', immediate='0000000000000000'):
        return [''.join([opcode, rs, rt, immediate])]
    
    def createJinst (self,opcode, addr='00000000000000000000000000'):
        return [''.join([opcode, addr])]


class InstructionTranslator:

    creator = InstructionCreator()

    def __init__(self, mnemonic):
        self.mnemonic = mnemonic.lower()


class RTranslator (InstructionTranslator):

    def __init__ (self, mnemonic):
        InstructionTranslator.__init__(self,mnemonic)
    
    def translate (self, args):
        inst = getInstInfo(self.mnemonic)
        opcode = inst['opcode']
        funct = inst['funct']
        defaults = inst['defaults']
        format = inst['format']

        instRaw = {
            'rs': 'zero',
            'rt': 'zero',
            'rd': 'zero',
            'shift': 0,
            'funct': 0
        }

        for index, arg, in enumerate(args):
            instRaw[format[index]] = arg
        
        rs = registers.getRegBinary(instRaw['rs'])
        rt = registers.getRegBinary(instRaw['rt'])
        rd = registers.getRegBinary(instRaw['rd'])
        shift = getBinaryRepresentation(int(instRaw['shift']),0,4)

        for key, value in defaults.iteritems():
            if key == 'shift':
                shift = value
            elif key == 'rs':
                rs = value
            elif key == 'rt':
                rt = value
            elif key == 'rd':
                rd = value

        return self.creator.createRinst(opcode,rs,rt,rd,shift,funct)



class ITranslator (InstructionTranslator):

    def __init__ (self, mnemonic):
        InstructionTranslator.__init__(self,mnemonic)

    def translate(self, args):
        inst = getInstInfo(self.mnemonic)
        opcode = inst['opcode']
        defaults = inst['defaults']
        format = inst['format']

        instRaw = {
            'rs': 'zero',
            'rt': 'zero',
            'immediate': 0
        }
        
        
        for index, arg, in enumerate(args):
            instRaw[format[index]] = arg

        rs = registers.getRegBinary(instRaw['rs'])
        rt = registers.getRegBinary(instRaw['rt'])
        immediate = getBinaryRepresentation(int(instRaw['immediate']),0,15)

        for key, value in defaults.iteritems():
            if key == 'immediate':
                immediate = value
            elif key == 'rs':
                rs = value
            elif key == 'rt':
                rt = value


        return self.creator.createIinst(opcode,rs,rt,immediate)


class JTranslator (InstructionTranslator):

    def __init__ (self, mnemonic):
        InstructionTranslator.__init__(self,mnemonic)

    def translate(self,args):
        inst = getInstInfo(self.mnemonic)
        opcode = inst['opcode']
        defaults = inst['defaults']
        format = inst['format']

        instRaw = {
            'address': 0,
        }

        for index, arg, in enumerate(args):
            instRaw[format[index]] = arg

        address = getBinaryRepresentation(int(instRaw['address']),0,25)

        for key, value in defaults.iteritems():
            if key == 'address':
                address = value
            

        return self.creator.createJinst(opcode,address)

class PseudoTranslator (InstructionTranslator):

    def __init__ (self, mnemonic):
        InstructionTranslator.__init__(self,mnemonic)
    
    def translate(self,args):
        info = pseudoinstructions.pseudoInsts[self.mnemonic]
        format = info['format']
        applyFunc = info['apply']

        instRaw = {
            'rs': 'zero',
            'rt': 'zero',
            'rd': 'zero',
            'immediate': 0,
            'label': 0
        }

        for index, arg, in enumerate(args):
            instRaw[format[index]] = arg

        return applyFunc(instRaw)

        