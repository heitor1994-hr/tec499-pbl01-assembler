from util import getBinaryRepresentation
from instructions import RTranslator,ITranslator,JTranslator
import registers

def move(itensDict):
    rTranslator = RTranslator('add')
    return rTranslator.translate([itensDict['rd'], itensDict['rs'], 'zero'])

def li(itensDict):
    upperImmediate = getBinaryRepresentation(int(itensDict['immediate']), 16, 31)
    lowerImmediate = getBinaryRepresentation(int(itensDict['immediate']), 0, 15)
    iTranslator = ITranslator('lui')
    lui = iTranslator.translate([itensDict['rd'], int(upperImmediate,2)])
    iTranslator = ITranslator('ori')
    ori = iTranslator.translate([itensDict['rd'], itensDict['rd'], int(lowerImmediate, 2)]) 
    return [lui[0], ori[0]]

def la(itensDict):
    iTranslator = ITranslator('addi')
    return iTranslator.translate([itensDict['rd'], 'zero', itensDict['label']])

def nop(itensDict):
    rTranslator = RTranslator('sll')
    return rTranslator.translate(['zero', 'zero', 0])

def _not(itensDict):
    rTranslator = RTranslator('nor')
    return rTranslator.translate([itensDict['rd'], itensDict['rs'], itensDict['rs']])

def negu(itensDict):
    rTranslator = RTranslator('subu')
    return rTranslator.translate([itensDict['rd'], 'zero', itensDict['rs']])

def blt(itensDict):
    rTranslator = RTranslator('slt')
    iTranslator = ITranslator('bne')
    slt = rTranslator.translate([ 'at', itensDict['rs'], itensDict['rt'] ])
    bne = iTranslator.translate([ 'at', 'zero', itensDict['label'] ])
    return [slt[0], bne[0]]

def blti(itensDict):
    iTranslator = ITranslator('slti')
    slti = iTranslator.translate([ 'at', itensDict['rs'], itensDict['immediate'] ])
    iTranslator = ITranslator('bne')
    bne = iTranslator.translate([ 'at', 'zero', itensDict['label'] ])
    return [slti[0], bne[0]]

def bgt(itensDict):
    rTranslator = RTranslator('slt')
    iTranslator = ITranslator('bne')

    slt = rTranslator.translate([ 'at', itensDict['rt'], itensDict['rs'] ])
    bne = iTranslator.translate([ 'at', 'zero', itensDict['label'] ])
    return [slt[0], bne[0]]

pseudoInsts = {
    'move': { 'format': ['rd', 'rs'], 'apply': move , 'size': 1 },
    'li': { 'format': ['rd', 'immediate'], 'apply': li, 'size': 2 },
    'la': { 'format': ['rd', 'label'], 'apply': la, 'size': 1 },
    'nop': { 'format': [], 'apply': nop, 'size': 1 } ,
    'not': { 'format': ['rd', 'rs'], 'apply': _not, 'size': 1 } ,
    'negu': { 'format': ['rd', 'rs'], 'apply': negu, 'size': 1 } ,
    'blt': { 'format': ['rs', 'rt', 'label'], 'apply': blt, 'size': 2 } ,
    'blti': { 'format': ['rs', 'immediate', 'label'], 'apply': blti, 'size': 2 },
    'bgt': { 'format': ['rs', 'rt', 'label'], 'apply': bgt, 'size': 2 }
}

def isPseudoInstruction(inst):
    if inst in pseudoInsts:
        return True
    else:
        return False

def getPseudoInstructionSize(inst):
    return pseudoInsts[inst]['size']


