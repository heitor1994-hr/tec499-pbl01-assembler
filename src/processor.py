import re
import os
import instructions
import pseudoinstructions
from util import getBinaryRepresentation
from config import assemblerConfig

symbolTable = {}

dataValues = []

# Regex Patterns
labelPattern = re.compile('[^0-9].+:')
varTypePattern = re.compile('.+:\s*(\.\w+)\s+')
numbersPattern = re.compile('[,\s]+([-\d]+)')
instructionLinePattern = re.compile('(\w+)\s*.*')

def addToSymbolTable(label, val):
    if label not in symbolTable and not label[0].isdigit():
        lastLabel = label
        symbolTable[label] = val


def collectCodeLabels(pseg):

    currentInstruction = assemblerConfig['textSegmentStart']

    for line in pseg.splitlines():
        labelMatch = labelPattern.match(line)
        instMatch = instructionLinePattern.match(line)

        #if the line has a label
        if labelMatch:
            label = labelMatch.group(0)[:-1]
            label = label.strip()
            addToSymbolTable(label, currentInstruction)
        
        #if the line has a instruction
        elif instMatch:
            mnemonic = instMatch.group(0).strip().split()[0]
            currentInstruction += instructions.getInstructionSize(mnemonic)
        
        if currentInstruction > assemblerConfig['textSegmentEnd']:
            print "Your Code is too long"
            return

def processDirective(directive, values):

    offset = 0

    if directive == '.word':
        for value in values:
            val = getBinaryRepresentation(int(value),0,31)
            dataValues.append(val)
            offset += 1
    elif directive == '.space':
        for value in values:
            space = int(value)/4
            for i in range(0,space):
                val = getBinaryRepresentation(0,0,31)
                dataValues.append(val)
                offset += 1
    
    return offset


def allocateData(dseg):

    varOffset = assemblerConfig['dataSegmentStart']


    for line in dseg.splitlines():
        labelMatch = labelPattern.match(line)
        directiveMatch = varTypePattern.match(line)
        numbersMatch = numbersPattern.findall(line)

        #if the line has a label
        if labelMatch:
            label = labelMatch.group(0)[:-1]
            label = label.strip()
            addToSymbolTable(label, varOffset*assemblerConfig['memOffset'])
        
        #if the line has a directive
        if directiveMatch:
            directive = directiveMatch.group(1)
            varOffset += processDirective(directive, numbersMatch)
            
        if varOffset > assemblerConfig['dataSegmentEnd']:
            print "Your code has too much data"
            return

    

def translateProgram(pseg):

    instructionList = []

    currentInstruction = 0


    for line in pseg.splitlines():
        instMatch = instructionLinePattern.match(line)
        labelMatch = labelPattern.match(line)

        #if label match, skip
        if labelMatch:
            continue

        if instMatch:
            # trims the instruction set
            match = instMatch.group(0).strip()
            
            # removes all $ symbols
            match = re.sub(r'\$', "", match)

            # removes all commas
            match = re.sub(r'\,', "", match)

            # splits the string on empty spaces
            # position 0 is the instruction label
            match = match.split()

            instruction = instructions.getInstructionTranslator(match[0])
     
            indexToChange = 1
            for operand in match[1:]:
                
                # checks whether the label is in the symbol's table or not
                # if true changes the label for its address on the match array
                if operand in symbolTable.keys():
                    operand = symbolTable[operand]
                    if instructions.isBranch(match[0]):
                        offset = operand - (currentInstruction+instructions.getInstructionSize(match[0]))
                        operand = offset
                    match[indexToChange] = operand
                
                # checks whether the operand is in the form x($y)
                # if true removes the ) and splits on the ( in order to separate x and y
                if "(" in str(operand):
                    operand = re.sub(r'\)', "", operand)
                    operand = operand.split("(")
                    match[indexToChange] = operand[0]
                    match.append(operand[1])
                indexToChange += 1
            
            translatedInstruction = instruction.translate(match[1:])
            instructionList.extend(translatedInstruction)
            currentInstruction += instructions.getInstructionSize(match[0])

            
    
    extendInstructionList(instructionList)
    return instructionList

def extendInstructionList(instList):
    length = len(instList)

    zero = getBinaryRepresentation(0,0,31)

    for i in range(length,assemblerConfig['textSegmentEnd']+1):
        instList.append(zero)

def appendDataSegment(instList):
    instList.extend(dataValues)
    return instList

def writeInstructions(filename, instList):
    file = None

    if assemblerConfig["hexOutput"]:
        file = createOutputFile(filename,".hmc")
        for inst in instList:
            hexInst = '%08X' % int(inst,2)
            file.write(hexInst)
            file.write('\n')
        print "Hexadecimal file created! ", file.name

    if assemblerConfig["binOutput"]:
        file = createOutputFile(filename,".mc")
        for inst in instList:
            file.write(inst)
            file.write('\n')
        print "Binary file created! " ,file.name
    
    if file != None:
        file.close()

def createOutputFile(filename, extension):
    outFilename = filename + extension
    if os.path.isfile(outFilename):
        os.remove(outFilename)
    
    return open(outFilename, 'w')

def startParsing(filename, dseg, pseg):    
    collectCodeLabels(pseg)
    allocateData(dseg)
    instructionList = translateProgram(pseg)
    instructionList = appendDataSegment(instructionList)
    writeInstructions(filename[:-4], instructionList)
