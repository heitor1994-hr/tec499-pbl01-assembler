from __future__ import print_function
from registers import getRegBinary
from preprocessor import process
from pseudoinstructions import pseudoInsts
import processor
import instructions
import sys
import tests




def main(filename):
    
    # preprocesses a given file in order to extract its .dseg and .pseg content
    preprocessed_file = process(filename)
    text = str(preprocessed_file['text'])
    data = str(preprocessed_file['data'])
    processor.startParsing(filename,data,text)

    #tests.run()

if __name__ == "__main__":
    main(sys.argv[1])
