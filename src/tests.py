import instructions
import registers



def assertEquals (text, v1, v2):
    print 'testing: ',text
    if v1 == v2:
        print 'OK'
    else:
        print 'FAIL, expected: ',v1, ' received: ',v2



def run():

    # test add at, v0, v1
    i = instructions.getInstructionTranslator('add')
    val = i.translate(['at','v0','v1'])
    assertEquals('add at, v0, v1', ['00000000010000110000100000100000'], val)

    #test seb at, v0
    i = instructions.getInstructionTranslator('seb')
    val = i.translate(['at','v0'])
    assertEquals('seb at, v0', ['01111100000000100000110000100000'], val)

    #test sll at, v0, 31
    i = instructions.getInstructionTranslator('sll')
    val = i.translate(['at','v0', '31'])
    assertEquals('sll at, v0, 31', ['00000000000000100000111111000000'], val)

    #test div at, v0
    i = instructions.getInstructionTranslator('div')
    val = i.translate(['at','v0'])
    assertEquals('div at, v0', ['00000000001000100000000000011010'], val)

    #test lui at, -50
    i = instructions.getInstructionTranslator('lui')
    val = i.translate(['at','-50'])
    assertEquals('lui at, -50', ['00111100000000011111111111001110'], val)

    #test lw at, 256(v0)
    i = instructions.getInstructionTranslator('lw')
    val = i.translate(['at','256', 'v0'])
    assertEquals('lw at, 256(v0)', ['10001100010000010000000100000000'], val)

    #test j 65535
    i = instructions.getInstructionTranslator('j')
    val = i.translate(['65535'])
    assertEquals('j 65535', ['00001000000000001111111111111111'], val)
    
    #test jal 67108863
    i = instructions.getInstructionTranslator('jal')
    val = i.translate(['67108863'])
    assertEquals('jal 67108863', ['00001111111111111111111111111111'], val)

    #test move at, v0
    i = instructions.getInstructionTranslator('move')
    val = i.translate(['at', 'v0'])
    assertEquals('move at, v0', ['00000000010000000000100000100000'], val)

    #test li at, 100000
    i = instructions.getInstructionTranslator('li')
    val = i.translate(['at', '100000'])
    assertEquals('li at, 100000', ['00111100000000010000000000000001', 
                                 '00110100001000011000011010100000'], val)
    #test nop
    i = instructions.getInstructionTranslator('nop')
    val = i.translate([])
    assertEquals('nop', ['00000000000000000000000000100000'], val)

