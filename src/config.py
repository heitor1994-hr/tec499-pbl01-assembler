

assemblerConfig = {
    "memOffset": 4,
    "textSegmentStart": 0,      # start of code segment in memory
    "textSegmentEnd": 4095,     # end of code segment in memory
    "dataSegmentStart": 4096,   # start of data segment in memory
    "dataSegmentEnd": 8191,      # end of data segment in memory
    "hexOutput": True,
    "binOutput": True
}