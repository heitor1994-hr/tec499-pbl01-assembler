import re


def removeEmptyLines(target):
  return re.sub(r'\n+', '\n', target)

def removeWhitespaceFromLineStart(target):
  return re.sub(r'\n(\s)+', '\n', target)

def removeWhitespaceAfterComma(target):
  return re.sub(r'\,( )*', ", ", target)

def removeWhitespaceExcess(target):
  return re.sub(r'[ \r\f\v\t]+', " ", target)

def removeComentaries(target):
  return re.sub(r'\#(.*)(?=\r?\n)', "", target)

def separateLabelInstruction(target):
  return re.sub(r'(.+:)\s*(\w+)',r'\1\n\2',target)

def process(filename):

    content = ""
    with open (filename, 'r') as f:
        for line in f:
	        content += line

    # removes any occurrence of a commentary on the source code
    content  = removeComentaries(content)
    
    # searches for the .data directive
    matcher_data = re.search('.data', content)

    # searches for the .text directive
    matcher_text = re.search('.text', content)
    
    data_content = ''
    text_content = ''

    if matcher_data is not None and matcher_text is not None:
      data_content = content[matcher_data.end():matcher_text.start()]

    if matcher_text is not None:
      text_content = content[matcher_text.end():]
    
    # eliminates extra space characters
    data_content = removeWhitespaceExcess(data_content)
    text_content = removeWhitespaceExcess(text_content)

    # guarantees that there is only one space character after a comma
    data_content = removeWhitespaceAfterComma(data_content)
    text_content = removeWhitespaceAfterComma(text_content)

    
    text_content = removeEmptyLines(text_content)
    text_content = removeWhitespaceFromLineStart(text_content)

    data_content = removeEmptyLines(data_content)
    data_content = removeWhitespaceFromLineStart(data_content)

    # if label and instruction are in the same line, push instruction to the next line
    text_content = separateLabelInstruction(text_content)


    preprocessed_file = {
			    "data" : data_content,
          "text" : text_content				
                }

    return preprocessed_file


